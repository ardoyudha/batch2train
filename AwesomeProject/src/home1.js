import {View, Text, TouchableOpacity} from 'react-native';
import React, {useEffect, useState} from 'react';

const Home1 = ({navigation}) => {
  const [count, setcount] = useState(0);

  const increment = () => {
    setcount(count + 1);
  };
  const decrement = () => {
    setcount(count - 1);
    if (count <= 0) {
      setcount(0);
    }
  };

  useEffect(() => {
    console.log(count);
  }, [count]);

  return (
    <View style={{flex: 1, justifyContent: 'center'}}>
      <Text
        style={{
          alignSelf: 'center',
          color: 'black',
          fontSize: 16,
          fontWeight: 'bold',
        }}>
        {count}
      </Text>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}>
        <TouchableOpacity
          onPress={() => {
            increment();
          }}>
          <Text
            style={{
              color: 'black',
            }}>
            Button 1{' '}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            navigation.navigate('Class');
          }}>
          <Text>Ke Class</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            decrement();
          }}>
          <Text
            style={{
              color: 'black',
            }}>
            Button 2
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default Home1;
