import {View, Text, BackHandler, StyleSheet, ScrollView} from 'react-native';
import Headerss from '../src/component/headerss';
import React, {useState, useEffect} from 'react';

const History = ({navigation}) => {
  const HandlerBack = () => {
    navigation.goBack();
    return true;
  };

  const [result, setResult] = useState([]);
  const getData = async () => {
    var raw = '';

    var requestOptions = {
      method: 'GET',
      body: raw,
      redirect: 'follow',
    };

    fetch(
      'https://test-app-49a79-default-rtdb.asia-southeast1.firebasedatabase.app/transaction.json',
      requestOptions,
    )
      .then(response => response.text())
      .then(response => {
        const value = JSON.parse(response);
        //console.log(value);
        setResult(Object.entries(value));
      })
      .catch(error => console.log('error', error));
  };

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', HandlerBack);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', HandlerBack);
    };
  }, []);

  useEffect(() => {
    getData();
  }, []);

  return (
    <View style={styles.container}>
      {/* <View>
        <Headerss title={'History'} />
      </View> */}
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.body}>
          {result.map((e, index) => {
            //console.log(index);
            return (
              <View style={styles.list} key={index}>
                {/* <Text style={styles.data}>NO : {index}</Text> */}
                <Text style={styles.data}>ID : {e[0]}</Text>
                <Text style={styles.data}>Tujuan : {e[1].target}</Text>
                <Text style={styles.data}>Tipe Transaksi : {e[1].type}</Text>
                <Text style={styles.data}>Biaya : {e[1].amount}</Text>
              </View>
            );
          })}
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  list: {
    marginVertical: 10,
    backgroundColor: 'green',
    borderRadius: 20,
    borderWidth: 2,
    borderColor: 'white',
    width: 300,
    height: 100,
    justifyContent: 'center',
    alignContent: 'center',
  },
  body: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  data: {
    fontWeight: '600',
    color: 'white',
    fontFamily: 'Montserrat-Regular',
    marginLeft: 18,
  },
  text: {
    fontFamily: 'Montserrat-Bold',
    fontSize: 25,
    color: 'white',
    marginHorizontal: 25,
    paddingBottom: 10,
  },
});

export default History;
