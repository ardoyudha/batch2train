import {
  View,
  Text,
  Platform,
  TextInput,
  StyleSheet,
  TouchableOpacity,
  Image,
} from 'react-native';
import React, {useState} from 'react';
import Mata2 from '../image/EyeClose.png';
import Mata from '../image/EyeOp.png';

const TextInputCos = ({
  placeholders,
  onChangeTexts,
  values,
  heights,
  Secure,
  Ispas,
  onPresss,
}) => {
  const [lokalText, setLokalText] = useState('');
  return (
    <View>
      <View
        style={{
          height: heights ? heights : 50,
          borderWidth: 1,
          borderRadius: 8,
          marginHorizontal: 10,
          flexDirection: 'row',
          paddingHorizontal: 5,
          alignItems: 'center',
          borderColor: 'aqua',
        }}>
        <TextInput
          placeholder={placeholders}
          placeholderTextColor={'black'}
          value={values}
          onChangeText={Text => {
            setLokalText(Text);
            onChangeTexts(Text);
          }}
          secureTextEntry={Secure}
          style={{width: '90%'}}
        />
        {Ispas ? (
          <>
            <TouchableOpacity
              style={{
                alignSelf: 'center',
                justifyContent: 'center',
                height: '100%',
                width: '10%',
              }}
              onPress={() => {
                onPresss();
              }}>
              <Image
                source={Secure == true ? Mata : Mata2}
                style={{
                  resizeMode: 'contain',
                  height: 30,
                  width: 30,
                  alignItems: 'center',
                }}
              />
            </TouchableOpacity>
          </>
        ) : (
          <></>
        )}
      </View>
    </View>
  );
};

const style = StyleSheet;

export default TextInputCos;
