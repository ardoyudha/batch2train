import React from 'react';
import {View, Modal, ActivityIndicator, StyleSheet, Image} from 'react-native';
import Proptypes from 'prop-types';

const Loading = ({isLoading, onRequestClose}) => {
  return (
    <Modal
      visible={isLoading}
      transparent={true}
      onRequestClose={onRequestClose}>
      <View style={style.container}>
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
          <Image
            source={{
              uri: 'https://media.tenor.com/g5ZphcWaj1MAAAAC/dancing-duck-vibing-duck.gif',
            }}
            style={{resizeMode: 'contain', height: 300, width: 300}}
          />
          {/* <ActivityIndicator size={50} color={'red'} animating={true} /> */}
        </View>
      </View>
    </Modal>
  );
};
// https://media.tenor.com/g5ZphcWaj1MAAAAC/dancing-duck-vibing-duck.gif
Loading.propTypes = {
  isLoading: Proptypes.bool.isRequired,
  onRequestClose: Proptypes.func,
};

const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.5)',
    justifyContent: 'center',
  },
  conLoad: {
    flexDirection: 'row',
    padding: 20,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    borderRadius: 10,
  },
});

export default Loading;
