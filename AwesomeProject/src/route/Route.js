// In App.js in a new project

import * as React from 'react';
import {View, Text} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Home from '../home';
import History from '../History';
import {Gobird, Goride, Gofood, Gocar} from './class';

const Stack = createNativeStackNavigator();

function Route() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Home"
        component={Home}
        options={{headerShown: false}}
      />
      <Stack.Screen name="Goride" component={Goride} />
      <Stack.Screen name="Gocar" component={Gocar} />
      <Stack.Screen name="Gofood" component={Gofood} />
      <Stack.Screen name="GoBird" component={Gobird} />
      <Stack.Screen
        name="History"
        component={History}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
}

export default Route;
