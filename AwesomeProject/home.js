import {
  View,
  Text,
  TouchableOpacity,
  Image,
  ImageBackground,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import TextInputCos from './src/component/TextInputCos';
import Loading from './src/component/Loading';

const home = () => {
  const [email, setEmail] = useState('');
  const [Texts, setTexts] = useState('');
  const [validasi, setValidasi] = useState(true);
  const [disables, setDisables] = useState(false);
  const [isload, setIsload] = useState(false);

  const ValidatorButton = () => {
    return email !== '' && Texts !== ''
      ? setDisables(false)
      : setDisables(true);
  };
  useEffect(() => {
    ValidatorButton();
  }, [email, Texts]);

  return (
    <View>
      <ImageBackground
        source={{
          uri: 'https://fastly.4sqi.net/img/user/130x130/NR22FJ5YUK33QN1Z.jpg',
        }}>
        <View style={{marginVertical: 50}} />
        <TextInputCos
          placeholders={'Masukkan Username'}
          values={email}
          onChangeTexts={ttt => {
            setEmail(ttt);
          }}
          Ispas={false}
        />
        <View style={{marginVertical: 10}} />
        <TextInputCos
          placeholders={'Masukkan Password'}
          values={Texts}
          onChangeTexts={ttt => {
            setTexts(ttt);
          }}
          Secure={validasi}
          onPresss={() => {
            setValidasi(!validasi);
          }}
          Ispas={true}
        />

        <View
          style={{
            marginHorizontal: 15,
          }}>
          <TouchableOpacity
            onPress={() => {
              setIsload(true);
              setTimeout(() => {
                setIsload(false);
              }, 3000);
            }}
            disabled={disables}
            style={{
              backgroundColor: disables == false ? 'white' : 'grey',
              paddingVertical: 10,
              marginTop: 10,
              alignItems: 'center',
              borderRadius: 10,
              elevation: 20,
            }}>
            <Text
              style={{
                color: 'black',
                fontSize: 16,
                fontWeight: 'bold',
              }}>
              confirm
            </Text>
          </TouchableOpacity>
        </View>
        <Loading isLoading={isload} />
      </ImageBackground>
    </View>
  );
};

export default home;
