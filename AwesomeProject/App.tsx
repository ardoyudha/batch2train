import { View, Text, TouchableOpacity } from 'react-native'
import React from 'react'
//import Home from './src/home2'
import { NavigationContainer } from '@react-navigation/native'
import Route from './src/route/Route'
import ScButton from './src/component/ScButton'

// const App = () => {
//   return (
//     <View style={{flex:1}}>
//       <Home/>
//     </View>
//   )
// }

const App = () => {
  return (
    <NavigationContainer>
      <Route />
    </NavigationContainer>
  )
}

// const Home = () => {
//   const data = [
//       { label: 'apaya', id: 0, },
//       { label: 'apaya 2', id: 1, },
//       { label: 'apaya 3', id: 2, },
//       { label: 'apaya 4', id: 3, },
//       { label: 'apaya 5', id: 4, },
//   ]

//   return (
//       <View style={{ flex: 1, justifyContent: 'center' }}>
//           <ScButton data={data} />
//       </View>
//   )
// }

export default App