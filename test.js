/** @format */

// /** @format */

// var a = 10;
// var b = "10";
// var c = String(a);
// var d = ["a", "b", "c", "d"];

// console.log(`ini adalah array ${d}`);

//pengenalan penutup
// () //invoke
// {} // curly bracket
// [] //array bracket
// <> // arrat tag
// => //arrow function

// const alpha = (a, b) => {
//   //return a + b;
//   return a > b
//     ? "a lebih besar dari b"
//     : a < 1
//     ? "isinya lebih kecil"
//     : "b lebih besar dari a";
// };

// console.log(alpha(0, 2));

//looping
// var n = "";

// for (var i = 1; i <= 9; i += 2) {
//   n += i + ",";
//   console.log(n);
// }

// for (let i = 1; i <= 5; i++) {
//   let spaces = "";
//   for (let j = 5 - i; j >= 1; j--) {
//     spaces += " ";
//   }
//   let asterisks = "";
//   for (let k = 1; k <= 2 * i - 1; k++) {
//     asterisks += "*";
//   }
//   console.log(spaces + asterisks);
// }

// for (let i = 4; i >= 1; i--) {
//   let spaces = "";
//   for (let j = 5 - i; j >= 1; j--) {
//     spaces += " ";
//   }
//   let asterisks = "";
//   for (let k = 1; k <= 2 * i - 1; k++) {
//     asterisks += "*";
//   }
//   console.log(spaces + asterisks);
// }

// While

// var jmlAngkot = 10;
// var noAngkot = 1;

// while (noAngkot <= jmlAngkot) {
//   console.log("Angkot " + noAngkot + " Beroperasi dengan baik");
//   noAngkot++;
// }

// const prompt = require("prompt-sync")();

// let a = prompt("enter a number: ");
// console.log(a);
// let n = 10;

// if (n % 2 == 0) {
//   for (var i = n; i < 10; i += 2) {
//     console.log(i);
//   }
// } else {
//   for (var i = n; i < 10; i += 2) {
//     console.log(i);
//   }
// }

// let n = 100;
// for (i = 1; i <= n; i++) {
//   if (i % 2 != 0) {
//     if (i <= 5) {
//       console.log(i);
//     }
//   }
// }

// challenge
// const prompt = require("prompt-sync")();
// let mtk = parseInt(prompt("Masukkan Nilai MTK: "));
// let indo = parseInt(prompt("Masukkan Nilai B.Indo: "));
// let ing = parseInt(prompt("Masukkan Nilai B.Inggris: "));
// let ipa = parseInt(prompt("Masukkan Nilai IPA: "));

// var avg = ((mtk + indo + ing + ipa) / 400) * 100;

// console.log("Output :");
// console.log("Rata-Rata = ", avg);

// if (avg < 60) {
//   console.log("Grade : E");
// } else if (avg < 70) {
//   console.log("Grade : D");
// } else if (avg < 80) {
//   console.log("Grade : C");
// } else if (avg < 90) {
//   console.log("Grade : B");
// } else if (avg < 100) {
//   console.log("Grade : A");
// } else {
//   console.log("Data tidak Valid");
// }

// const price = 1000;

// let USDollar = new Intl.NumberFormat("id-ID", {
//   style: "currency",
//   currency: "IDR",
// });

// console.log(`${USDollar.format(price)}`);

// hari ke 2

// const arr = ["sleep", "work", "exercise"];
// const arr1 = ["eat"];

// let arrnew = arr.sort();
// let arrpush = arr1.concat(arrnew);
// console.log(arrpush);

// var arr1 = [3, "a", "a", "a", 2, 3, "a", 3, "a", 2, 4, 9, 3];
// var mf = 1;
// var m = 0;
// var item;
// for (var i = 0; i < arr1.length; i++) {
//   for (var j = i; j < arr1.length; j++) {
//     if (arr1[i] == arr1[j]) m++;
//     if (mf < m) {
//       mf = m;
//       item = arr1[i];
//     }
//   }
//   m = 0;
// }
// console.log(item + " ( " + mf + " times ) ");

// let arr = [15.5, 2.3, 1.1, 4.7];

// function getSum(total, num) {
//   return total + Math.round(num);
// }

// console.log(arr.reduce(getSum, 0));

// function ValueElement(arr, k) {
//   var result = [];
//   for (var i = 0; i < arr.length; i++) {
//     if (arr[i] % k === 0) {
//       result.push(arr[i]);
//     }
//   }
//   return result;
// }

// console.log(ValueElement([1, 2, 3, 4, 5, 6, 7], 2)); // output: [2, 4, 6]

// const weekdays = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'];
// const ChoseText = PickerText(weekdays);

// function PickerText(weekdays) {
//   return function ChoseText(number){

//   }
// }

// function getWeekday(index) {
//   if (index >= 0 && index <= 4) {
//     return inputArray[index];
//   } else {
//     return "ERROR: Invalid weekday number";
//   }
// }

// console.log(getWeekday(0)); // Output: "Monday"
// console.log(getWeekday(3)); // Output: "Thursday"
// console.log(getWeekday(5)); // Output: "ERROR: Invalid weekday number"

function birthday(s, d, m) {
  let sum = 0;
  let count = 0;
  for (let i = 0; i < s.length; i++) {
    sum += s[i];
    if (i >= m) {
      sum -= s[i - m];
    }
    if (i >= m - 1 && sum === d) {
      count++;
    }
  }
  return count;
}

console.log(birthday([2, 2, 1, 3, 2], 4, 2)); // Output: 2
console.log(birthday([1, 2, 1, 3, 2], 3, 2)); // Output: 2
console.log(birthday([1, 1, 1, 1, 1], 3, 2)); // Output: 0
console.log(birthday([4], 4, 1)); // Output: 1
